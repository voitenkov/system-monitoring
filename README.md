# About the Project

**System Monitoring** is a demo application written with Golang.
This application was developed to demonstrate:
- simple GRPC server
- simple GRPC client 
- examples of GRPC streaming
- examples of collecting system metrics
- CI/CD process based on Gitlab CI

![Reference](./img/server.png)
![Reference](./img/sysload.png)
![Reference](./img/cpuload.png)
![Reference](./img/diskload.png)
![Reference](./img/diskuse.png)


## Contributing

Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a merge request. You can also simply open an issue with the tag "enhancement".

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Merge Request


## Contact

Andrey Voytenkov - avoytenkov@yandex.ru

Project link: [https://gitlab.com/voitenkov/system-monitoring](https://gitlab.com/voitenkov/system-monitoring)



