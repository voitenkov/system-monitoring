package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"net"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"time"

	"gitlab.com/voitenkov/system-monitoring/internal/client"
)

var (
	address                    string
	ctxMain                    context.Context
	timeout                    time.Duration
	statType                   string
	interval, rate             int
	stop                       context.CancelFunc
	err                        error
	ErrNoArguments             = errors.New("no arguments provided")
	ErrNoPortProvided          = errors.New("no port provided")
	ErrExtraArgumentsProvided  = errors.New("extra arguments provided")
	ErrPortNotNumeric          = errors.New("port is not numeric")
	ErrPortOutOfBounds         = errors.New("port should be from 1 to 65535")
	ErrTerminatedByUser        = errors.New("connection terminated by user")
	ErrTerminatedByServer      = errors.New("connection terminated by peer")
	ErrNoConnectionEstablished = errors.New("no connection established")
)

func init() {
	flag.DurationVar(&timeout, "timeout", time.Second*10, "connection timeout")
	flag.StringVar(&statType, "type", "sys_load", "statistics type")
	flag.IntVar(&interval, "interval", 5, "request interval, seconds")
	flag.IntVar(&rate, "rate", 15, "sample rate, seconds")
}

func main() {
	ctxMain, stop = signal.NotifyContext(context.Background(), os.Interrupt)
	defer stop()
	go func() {
		<-ctxMain.Done()
		fmt.Println(ctxMain.Err())
		os.Exit(1)
	}()

	flag.Parse()
	argsCount := len(flag.Args())

	switch {
	case argsCount == 0:
		err = ErrNoArguments
	case argsCount == 1 && flag.Arg(0) != "version":
		err = ErrNoPortProvided
	case argsCount == 2:
		port, converr := strconv.Atoi(flag.Arg(1))
		if converr != nil {
			err = ErrPortNotNumeric
		} else if port < 1 || port > 65535 {
			err = ErrPortOutOfBounds
		}
	case argsCount > 2:
		err = ErrExtraArgumentsProvided
	}

	if err != nil {
		fmt.Println(err)
		return
	}

	if flag.Arg(0) == "version" && argsCount == 1 {
		printVersion()
		return
	}

	address = net.JoinHostPort(flag.Arg(0), flag.Arg(1))
	client := client.NewClient(address, timeout, statType, interval, rate)
	if client == nil {
		panic("sysmon client failed")
	}

	ctx, cancel := context.WithTimeout(ctxMain, timeout)
	defer cancel()
	err = client.Connect(ctx)
	if err != nil {
		fmt.Println(err)
		cancel()
		os.Exit(1) //nolint:gocritic
	}

	wg := &sync.WaitGroup{}

	wg.Add(1)
	go func() {
		defer wg.Done()
		err := client.Receive(ctxMain)
		if err != nil {
			fmt.Fprintln(os.Stderr, "...connection was closed by peer")
			fmt.Println(err)
			client.Close()
			stop()
			os.Exit(1)
		}
	}()

	wg.Wait()
	client.Close()
}
