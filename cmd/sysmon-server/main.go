package main

import (
	"context"
	"flag"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"

	"gitlab.com/voitenkov/system-monitoring/internal/app"
	collectors "gitlab.com/voitenkov/system-monitoring/internal/collectors/init"
	"gitlab.com/voitenkov/system-monitoring/internal/config"
	"gitlab.com/voitenkov/system-monitoring/internal/logger"
	"gitlab.com/voitenkov/system-monitoring/internal/server"
)

var (
	configFile string
	wg         *sync.WaitGroup
)

func init() {
	flag.StringVar(&configFile, "config", "/etc/sysmon/config.yaml", "Path to configuration file")
}

func main() {
	flag.Parse()

	if flag.Arg(0) == "version" {
		printVersion()
		return
	}

	cfg, err := config.Parse(configFile)
	if err != nil {
		log.Fatal(err)
	}

	logg := logger.New(cfg.Logger.Level)

	collectors, err := collectors.New(cfg)
	if err != nil {
		log.Fatal(err)
	}

	sysmon := app.New(collectors)
	server := server.NewServer(logg, sysmon, cfg)

	ctx, cancel := signal.NotifyContext(context.Background(),
		syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer cancel()

	go func() {
		<-ctx.Done()
		server.Stop()
	}()

	logg.Info("sysmon is running...")
	wg = &sync.WaitGroup{}

	wg.Add(1)
	go func() {
		defer wg.Done()
		if err := server.Start(ctx); err != nil {
			logg.Error("failed to start grpc server: " + err.Error())
		}
	}()

	wg.Wait()
	cancel()
	os.Exit(1) //nolint:gocritic
}
