SERVER=sysmon-server
CLIENT=sysmon-client
REPOSITORY=voitenkov
VERSION=0.1.0
GIT_HASH := $(shell git log --format="%h" -n 1)
LDFLAGS := -X main.release="develop" -X main.buildDate=$(shell date -u +%Y-%m-%dT%H:%M:%S) -X main.gitHash=$(GIT_HASH)

.PHONY: build-server
build-server:
	go build -v -o ./bin/$(SERVER) -ldflags "$(LDFLAGS)" ./cmd/$(SERVER)

.PHONY: build-client
build-client:
	go build -v -o ./bin/$(CLIENT) -ldflags "$(LDFLAGS)" ./cmd/$(CLIENT)

.PHONY: build
build:
	go build -v -o ./bin/$(SERVER) -ldflags "$(LDFLAGS)" ./cmd/$(SERVER)
	go build -v -o ./bin/$(CLIENT) -ldflags "$(LDFLAGS)" ./cmd/$(CLIENT)

.PHONY: generate
generate:
	go generate ./...

.PHONY: run-server
run-server: build-server
	./bin/$(SERVER) -config ./configs/config.yaml

.PHONY: run-client
run-client: build-client
	./bin/$(CLIENT) -type cpu_load 127.0.0.1 8080

.PHONY: image-server
image-server:
	docker build \
		--build-arg=LDFLAGS="$(LDFLAGS)" \
		-f build/$(SERVER)/Dockerfile \
		--tag $(REPOSITORY)/$(SERVER):$(VERSION) \
		--tag $(REPOSITORY)/$(SERVER):latest \
		.

.PHONY: image-client
image-client:
	docker build \
		--build-arg=LDFLAGS="$(LDFLAGS)" \
		-f build/$(CLIENT)/Dockerfile \
		--tag $(REPOSITORY)/$(CLIENT):$(VERSION) \
		--tag $(REPOSITORY)/$(CLIENT):latest \
		.

.PHONY: docker-server
docker-server: image-server
	docker network create -d bridge sysmon_network || true
	docker run \
	--name sysmon-server \
	--hostname sysmon-server \
	--network sysmon_network \
	$(REPOSITORY)/$(SERVER):$(VERSION)

.PHONY: docker-client
docker-client: image-client
	docker network create -d bridge sysmon_network || true
	docker run \
	--name sysmon-client \
	--hostname sysmon-client \
	--network sysmon_network \
	$(REPOSITORY)/$(CLIENT):$(VERSION) sysmon-server 8080

.PHONY: version-server
version-server: build-server
	./bin/$(SERVER) version

.PHONY: version-client
version-client: build-client
	./bin/$(CLIENT) version

.PHONY: test
test:
	go test -race -count 1 ./internal/...

.PHONY: install-lint-deps
install-lint-deps:
	(which golangci-lint > /dev/null) || curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(shell go env GOPATH)/bin v1.55.2

.PHONY: lint
lint: install-lint-deps
	golangci-lint run ./...

.PHONY: up
up:
	docker compose -f deployments/docker-compose.yaml up -d

.PHONY: down
down:
	docker compose -f deployments/docker-compose.yaml down

.PHONY: integration-tests
integration-tests:
	docker-compose -f deployments/docker-compose_test.yaml up --abort-on-container-exit --exit-code-from sysmon-client-test sysmon-client-test
	docker-compose -f deployments/docker-compose_test.yaml down