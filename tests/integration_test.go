package tests

import (
	"context"
	"flag"
	"fmt"
	"net"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/suite"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/metadata"

	"gitlab.com/voitenkov/system-monitoring/api/proto"
)

type GRPCTestSuite struct {
	suite.Suite
	client  *testClient
	request *proto.SysinfoRequest
}

type testClient struct {
	clnt     proto.SysinfoServiceClient
	conn     *grpc.ClientConn
	address  string
	timeout  time.Duration
	statType string
	interval int
	rate     int
}

var (
	timeout        time.Duration
	statType       string
	interval, rate int
	ctx            context.Context
	cancel         context.CancelFunc
)

const clientID = "14e4a342-2ad9-4e1f-bd83-eff99332a49f"

func init() {
	flag.DurationVar(&timeout, "timeout", time.Second*10, "connection timeout")
	flag.StringVar(&statType, "type", "sys_load", "statistics type")
	flag.IntVar(&interval, "interval", 5, "request interval, seconds")
	flag.IntVar(&rate, "rate", 15, "sample rate, seconds")
}

func TestGRPCTestSuite(t *testing.T) {
	suite.Run(t, &GRPCTestSuite{})
}

func (s *GRPCTestSuite) SetupSuite() {
	flag.Parse()
	address := net.JoinHostPort(flag.Arg(0), flag.Arg(1))
	s.client = newTestClient(address, timeout, statType, interval, rate)
	s.NotNil(s.client)
	ctx, cancel = context.WithCancel(context.Background())
	err := s.client.Connect(ctx)
	s.NoError(err)
}

func (s *GRPCTestSuite) SetupTest() {
	s.request = &proto.SysinfoRequest{
		StatType:        s.client.statType,
		IntervalSeconds: int32(s.client.interval),
		RateSeconds:     int32(s.client.rate),
	}
}

func newTestClient(address string, timeout time.Duration, statType string, interval, rate int) *testClient {
	return &testClient{
		address:  address,
		timeout:  timeout,
		statType: statType,
		interval: interval,
		rate:     rate,
	}
}

func (c *testClient) Connect(ctx context.Context) error {
	var err error

	c.conn, err = grpc.DialContext(ctx, c.address, grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithBlock())

	if err != nil {
		return fmt.Errorf("cannot connect to %v: %w", c.address, err)
	}

	fmt.Fprintln(os.Stderr, "...Connected to", c.address)
	c.clnt = proto.NewSysinfoServiceClient(c.conn)
	return nil
}

func (s *GRPCTestSuite) TestNoUserID() {
	s.NotNil(s.client.clnt)
	stream, err := s.client.clnt.GetSysinfo(ctx, s.request)
	s.NoError(err)
	_, err = stream.Recv()
	s.Error(err)
}

func (s *GRPCTestSuite) TestStream() {
	s.NotNil(s.client.clnt)
	md := metadata.New(nil)
	md.Append("client_id", clientID)
	ctxLocal := metadata.NewOutgoingContext(ctx, md)
	stream, err := s.client.clnt.GetSysinfo(ctxLocal, s.request)
	s.NoError(err)

	for i := 0; i < 5; i++ {
		response, err := stream.Recv()
		s.NoError(err)
		s.Equal(len(response.Keys), 3)
		s.Equal(response.Keys[0], "load average (1)")
		s.Equal(response.Keys[1], "load average (5)")
		s.Equal(response.Keys[2], "load average (15)")
		_, ok := response.Values["load average (1)"]
		s.True(ok)
		_, ok = response.Values["load average (5)"]
		s.True(ok)
		_, ok = response.Values["load average (15)"]
		s.True(ok)
	}
	s.client.conn.Close()
	s.client = nil
	s.request = nil
	cancel()
}
