package logger

import (
	"context"
	"errors"
	"log"
	"net"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/peer"
)

var (
	ErrCreatingDirectory     = errors.New("could not create directory")
	ErrCreatingFile          = errors.New("could not create file")
	ErrWritingFile           = errors.New("could not write to file")
	ErrRunningCommand        = errors.New("could not run command")
	ErrEmptyCommand          = errors.New("empty command")
	ErrReadingDir            = errors.New("error reading directory")
	ErrParsingValue          = errors.New("error parsing value")
	ErrCollectorIsNotEnabled = errors.New("collector is not enabled")
	ErrGettingValuesFromFile = errors.New("error getting values from file")
)

type Logger struct{}

func New(level string) *Logger {
	logLevel, err := logrus.ParseLevel(level)
	if err != nil {
		log.Fatalf("failed to parse the level: %v", err)
	}

	logrus.SetLevel(logLevel)
	return &Logger{}
}

func (l Logger) Info(msg ...interface{}) {
	logrus.Info(msg...)
}

func (l Logger) Infof(format string, args ...interface{}) {
	logrus.Infof(format, args...)
}

func (l Logger) Error(msg ...interface{}) {
	logrus.Error(msg...)
}

func (l Logger) Errorf(format string, args ...interface{}) {
	logrus.Infof(format, args...)
}

func (l Logger) Warn(msg ...interface{}) {
	logrus.Warn(msg...)
}

func (l Logger) Debug(msg ...interface{}) {
	logrus.Debug(msg...)
}

func (l Logger) LogGRPCRequest(ctx context.Context, info *grpc.UnaryServerInfo, duration time.Duration,
	statusCode string,
) {
	peer, ok := peer.FromContext(ctx)
	if !ok {
		logrus.Errorf("error receiving peer information: %s", peer.Addr.String())
	}

	userAgent := ""
	metadata, ok := metadata.FromIncomingContext(ctx)
	if ok {
		userAgent = strings.Join(metadata["user-agent"], " ")
	}

	ip, _, err := net.SplitHostPort(peer.Addr.String())
	if err != nil {
		logrus.Errorf("error splitting host and port: %q", peer.Addr.String())
	}

	formatString := "%s [%s] %s %s %s %s %d %s"
	t := time.Now().Format("02/Jan/2006:15:04:05 -0700")
	logrus.Infof(formatString, ip, t, "rpc", info.FullMethod, "HTTP/2", statusCode, duration.Microseconds(), userAgent)
}
