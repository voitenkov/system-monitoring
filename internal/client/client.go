package client

import (
	"context"
	"errors"
	"fmt"
	"io"
	"os"
	"time"

	"github.com/gofrs/uuid"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/metadata"

	"gitlab.com/voitenkov/system-monitoring/api/proto"
)

type Client interface {
	Connect(ctx context.Context) error
	Receive(ctx context.Context) error
	io.Closer
}

type client struct {
	clnt     proto.SysinfoServiceClient
	conn     *grpc.ClientConn
	address  string
	timeout  time.Duration
	statType string
	interval int
	rate     int
}

const (
	colorReset  = "\033[0m"
	colorGreen  = "\033[32m"
	colorYellow = "\033[33m"
)

var (
	ErrNoArguments             = errors.New("no arguments provided")
	ErrNoPortProvided          = errors.New("no port provided")
	ErrExtraArgumentsProvided  = errors.New("extra arguments provided")
	ErrPortNotNumeric          = errors.New("port is not numeric")
	ErrPortOutOfBounds         = errors.New("port should be from 1 to 65535")
	ErrTerminatedByUser        = errors.New("connection terminated by user")
	ErrTerminatedByServer      = errors.New("connection terminated by peer")
	ErrNoConnectionEstablished = errors.New("no connection established")
)

func (c *client) Connect(ctx context.Context) error {
	var err error

	c.conn, err = grpc.DialContext(ctx, c.address, grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithBlock())
	if err != nil {
		return fmt.Errorf("cannot connect to %v: %w", c.address, err)
	}

	fmt.Fprintln(os.Stderr, "...Connected to", c.address)
	c.clnt = proto.NewSysinfoServiceClient(c.conn)
	return nil
}

func (c *client) Receive(ctx context.Context) error {
	if c.clnt == nil {
		return ErrNoConnectionEstablished
	}

	req := &proto.SysinfoRequest{
		StatType:        c.statType,
		IntervalSeconds: int32(c.interval),
		RateSeconds:     int32(c.rate),
	}
	md := metadata.New(nil)
	clientID, err := uuid.NewV4()
	if err != nil {
		return fmt.Errorf("error generating clientid: %w", err)
	}

	md.Append("client_id", clientID.String())
	ctxLocal := metadata.NewOutgoingContext(ctx, md)

	stream, err := c.clnt.GetSysinfo(ctxLocal, req)
	if err != nil {
		return fmt.Errorf("error creating stream: %w", err)
	}

	for {
		select {
		case <-ctx.Done():
			return ErrTerminatedByUser
		default:
			response, err := stream.Recv()
			if err != nil {
				return fmt.Errorf("error receiving: %w", err)
			}

			fmt.Fprintf(os.Stdout, colorGreen+"%s"+colorReset+"\u2502", response.Time.AsTime().Format(time.TimeOnly))
			for _, key := range response.Keys {
				fmt.Fprintf(os.Stdout, colorYellow+"%v: "+colorReset+"%.2f\u2502", key, response.Values[key])
			}

			fmt.Println()
		}
	}
}

func (c *client) Close() error {
	if c.clnt == nil {
		return ErrNoConnectionEstablished
	}
	return c.conn.Close()
}

func NewClient(address string, timeout time.Duration, statType string, interval, rate int) Client {
	return &client{
		address:  address,
		timeout:  timeout,
		statType: statType,
		interval: interval,
		rate:     rate,
	}
}
