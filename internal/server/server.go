package server

import (
	"context"
	"net"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"

	"gitlab.com/voitenkov/system-monitoring/api/proto"
	"gitlab.com/voitenkov/system-monitoring/internal/app"
	"gitlab.com/voitenkov/system-monitoring/internal/config"
)

var msg *proto.SysinfoResponse

type Server struct {
	proto.UnimplementedSysinfoServiceServer
	host   string
	port   string
	logger Logger
	app    Application
	server *grpc.Server
}

type Logger interface {
	Error(msg ...interface{})
	Errorf(format string, args ...interface{})
	Info(msg ...interface{})
	Infof(format string, args ...interface{})
	Warn(msg ...interface{})
	Debug(msg ...interface{})
	LogGRPCRequest(ctx context.Context, info *grpc.UnaryServerInfo, duration time.Duration, statusCode string)
}

type Application interface {
	StartReader(statType app.StatType, stop chan bool) error
	StartParser(statType app.StatType, stop chan bool, rateSeconds int) error
	GetStat(statType app.StatType, rateSeconds int) (*proto.SysinfoResponse, error)
	CheckCollector(statType app.StatType) error
}

func NewServer(logger Logger, app Application, cfg *config.Config) *Server {
	return &Server{
		host:   cfg.Server.Host,
		port:   cfg.Server.Port,
		logger: logger,
		app:    app,
	}
}

func (s *Server) Start(ctx context.Context) error {
	server := grpc.NewServer(grpc.UnaryInterceptor(s.loggingInterceptor))
	s.server = server
	proto.RegisterSysinfoServiceServer(server, s)

	addr := net.JoinHostPort(s.host, s.port)

	go func() {
		listener, err := net.Listen("tcp", addr)
		if err != nil {
			s.logger.Error(err)
			return
		}

		err = server.Serve(listener)
		if err != nil {
			s.logger.Error(err)
		}
	}()

	s.logger.Info("GRPC server starting on tcp://" + addr)

	<-ctx.Done()
	return nil
}

func (s *Server) Stop() {
	s.server.GracefulStop()
}

//nolint:gocognit // don't want to split business logic
func (s *Server) GetSysinfo(request *proto.SysinfoRequest, srv proto.SysinfoService_GetSysinfoServer) error {
	var (
		clientID, statType string
		err                error
	)

	ctx := srv.Context()
	if ctx == nil {
		return status.Error(codes.InvalidArgument, "context is missing in request")
	}

	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return status.Error(codes.InvalidArgument, "error getting metadata from request context")
	}

	ids := md.Get("client_id")

	if len(ids) > 0 {
		clientID = ids[0]
	}

	statType = request.StatType
	if statType != "sys_load" &&
		statType != "cpu_load" &&
		statType != "disk_load" &&
		statType != "disk_use" {
		return status.Error(codes.NotFound, "unknown statistics type")
	}

	if clientID == "" {
		return status.Error(codes.Unauthenticated, "anonymous authentification is prohibited")
	}

	err = s.app.CheckCollector(app.StatType(statType))
	if err != nil {
		return status.Error(codes.Unavailable, "collector is not enabled")
	}

	s.logger.Infof("new stats listener %q for %q collector", clientID, statType)
	stopReader := make(chan bool)
	go func() {
		defer func() { stopReader <- true }()
		err := s.app.StartReader(app.StatType(request.StatType), stopReader)
		if err != nil {
			s.logger.Errorf("collector %q for client %q failed to read", statType, clientID)
		}
	}()

	time.Sleep(time.Second)
	stopParser := make(chan bool)
	go func() {
		defer func() { stopParser <- true }()
		err := s.app.StartParser(app.StatType(request.StatType), stopParser, int(request.RateSeconds))
		if err != nil {
			s.logger.Errorf("collector %q for client %q failed to parse", statType, clientID)
		}
	}()

	time.Sleep(time.Second * time.Duration(request.RateSeconds))
	ticker := time.NewTicker(time.Second * time.Duration(request.IntervalSeconds))
	var errorsCount int

L:
	for {
		select {
		case <-ctx.Done():
			s.logger.Infof("stats listener %q disconnected", clientID)
			break L
		case <-stopReader:
			ticker.Stop()
			stopParser <- true
			<-stopParser
			return status.Error(codes.Internal, "collector failed to read")
		case <-stopParser:
			ticker.Stop()
			stopReader <- true
			<-stopReader
			return status.Error(codes.Internal, "collector failed to parse")
		case <-ticker.C:
			//nolint:nestif // don't want to split business logic
			if errorsCount < 5 {
				msg, err = s.app.GetStat(app.StatType(request.StatType), int(request.RateSeconds))
				if err != nil || msg == nil {
					s.logger.Errorf("getting statistics of collector %q for client %q failed", request.StatType, clientID)
					errorsCount++
				} else {
					errorsCount = 0
					if err := srv.Send(msg); err != nil {
						s.logger.Errorf("unable to send message to stats listener: %v", err)
						break L
					}
				}
			} else {
				s.logger.Errorf("too many errors, closing stats listener %q", clientID)
				err = status.Error(codes.Internal, "too many errors")
				break L
			}
		}
	}

	ticker.Stop()
	stopReader <- true
	<-stopReader
	stopParser <- true
	<-stopParser

	return err
}
