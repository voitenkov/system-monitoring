package config

import (
	"log"
	"os"

	yaml "gopkg.in/yaml.v3"
)

type Config struct {
	Logger     LoggerConf
	Server     ServerConf
	Collectors CollectorsConf
}

type LoggerConf struct {
	Level string
}

type ServerConf struct {
	Host string
	Port string
}

type CollectorsConf struct {
	SysLoad SysLoadConf `yaml:"sysLoad"`
}

type SysLoadConf struct {
	Enabled bool
}

func NewConfig() *Config {
	return &Config{}
}

func Parse(filePath string) (*Config, error) {
	configData, err := os.ReadFile(filePath)
	if err != nil {
		log.Fatal(err)
	}

	cfg := NewConfig()
	err = yaml.Unmarshal(configData, cfg)
	if err != nil {
		return nil, err
	}

	return cfg, err
}
