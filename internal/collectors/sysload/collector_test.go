package sysload

import (
	"bufio"
	"os"
	"path/filepath"
	"strconv"
	"testing"
	"time"

	"github.com/stretchr/testify/require"

	"gitlab.com/voitenkov/system-monitoring/internal/collectors"
)

func TestReader(t *testing.T) {
	var (
		recordTime string
		err        error
	)

	fileNameSlice := make([]string, 0, 5)
	filePath := "/tmp/test"
	rateSecondsMax := 5
	keys := []collectors.Key{"key 1", "key 2", "key 3"}
	records := make(collectors.Records)

	t.Run("Reader test", func(t *testing.T) {
		ticker := time.NewTicker(time.Second)
		for i := 1; i <= 5; i++ {
			<-ticker.C
			valuesString := strconv.FormatFloat(float64(i)+0.2, 'g', -1, 64) +
				" " + strconv.FormatFloat(float64(i)+0.2, 'g', -1, 64) +
				" " + strconv.FormatFloat(float64(i)+0.2, 'g', -1, 64)
			recordTime, err = collectors.Reader("test", "echo "+valuesString)
			require.NoError(t, err)
			fileNameSlice = append(fileNameSlice, recordTime)
		}

		for i := 1; i <= 5; i++ {
			filePathWithName := filepath.Join(filePath, fileNameSlice[i-1])
			fileInfo, err := os.Stat(filePathWithName)
			require.NoError(t, err)
			require.NotEqual(t, 0, fileInfo.Size())

			file, err := os.Open(filePathWithName)
			require.NoError(t, err)
			scanner := bufio.NewScanner(file)
			scanner.Scan()
			valuesString := strconv.FormatFloat(float64(i)+0.2, 'g', -1, 64) +
				" " + strconv.FormatFloat(float64(i)+0.2, 'g', -1, 64) +
				" " + strconv.FormatFloat(float64(i)+0.2, 'g', -1, 64)
			require.Equal(t, valuesString, scanner.Text())
		}
	})

	t.Run("Parser test", func(t *testing.T) {
		for idx, fileName := range fileNameSlice {
			currentTime, err := collectors.GetRecordTimeFromString(recordTime)
			require.NoError(t, err)
			_, values, err := collectors.GetValuesFromFile(filePath, fileName, recordTime, rateSecondsMax,
				currentTime, keys, parser)
			if idx < 4 {
				require.NoError(t, err)
				valuesValue := *values
				require.Equal(t, float32(idx+1)+0.2, float32(valuesValue[collectors.Key("key 1")]))
				require.Equal(t, float32(idx+1)+0.2, float32(valuesValue[collectors.Key("key 2")]))
				require.Equal(t, float32(idx+1)+0.2, float32(valuesValue[collectors.Key("key 3")]))
				records[collectors.RecordTime(fileName)] = values
			}
		}
	})

	os.RemoveAll(filePath)
}
