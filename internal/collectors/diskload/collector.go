package diskload

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"google.golang.org/protobuf/types/known/timestamppb"

	"gitlab.com/voitenkov/system-monitoring/api/proto"
	"gitlab.com/voitenkov/system-monitoring/internal/collectors"
	"gitlab.com/voitenkov/system-monitoring/internal/logger"
)

type Collector struct {
	sync.RWMutex
	ClientsQty     int
	ReaderStarted  bool
	ParserStarted  bool
	LastRecordTime string
	RateSeconds    int
	Keys           []collectors.Key
	Records        collectors.Records
}

func (c *Collector) StartReader(stop chan bool) error {
	var err error

	c.Lock()
	c.ClientsQty++
	mainReader := !c.ReaderStarted
	if mainReader {
		c.ReaderStarted = true
	}

	c.Unlock()
	ticker := time.NewTicker(time.Second)
	for {
		select {
		case <-ticker.C:
			c.Lock()
			if mainReader {
				time := time.Now().Format(time.DateTime)
				recordTime := strings.ReplaceAll(strings.ReplaceAll(strings.ReplaceAll(time, "-", ""), " ", ""), ":", "")
				lastRecordTime := c.LastRecordTime
				c.LastRecordTime = recordTime
				recordTime, err = collectors.Reader("diskload", "iostat -d -k")
				if err != nil {
					err = fmt.Errorf("collector failed to read: %w", err)
					c.LastRecordTime = lastRecordTime
					c.ReaderStarted = false
					c.ClientsQty--
					return err
				}

				c.LastRecordTime = recordTime
			} else if !c.ReaderStarted {
				// Получился алгоритм автоматического выбора лидера.
				mainReader = true
				c.ReaderStarted = true
			}

			c.Unlock()
		case <-stop:
			c.Lock()
			c.ReaderStarted = false
			c.ClientsQty--
			c.Unlock()
			return nil
		}
	}
}

func (c *Collector) StartParser(stop chan bool, rateSeconds int) error {
	var lastRecordTime string

	c.Lock()
	readerStarted := c.ReaderStarted
	parserStarted := c.ParserStarted
	mainParser := readerStarted && !parserStarted
	if mainParser {
		c.ParserStarted = true
	}

	if rateSeconds > c.RateSeconds {
		c.RateSeconds = rateSeconds
	}

	c.Unlock()
	keys := []collectors.Key{}
	ticker := time.NewTicker(time.Second)
	for {
		select {
		case <-ticker.C:
			c.Lock()
			//nolint:nestif // don't want to split business logic
			if mainParser {
				currentTimeString := time.Now().Format(time.DateTime)
				currentTimeStringCleaned := strings.ReplaceAll(strings.ReplaceAll(strings.ReplaceAll(currentTimeString,
					"-", ""), " ", ""), ":", "")
				currentTime, _ := collectors.GetRecordTimeFromString(currentTimeStringCleaned)
				filePath := "/tmp/diskload"
				dirEntries, err := os.ReadDir(filePath)
				if err != nil {
					return logger.ErrReadingDir
				}

				lastRecordTime = c.LastRecordTime
				rateSecondsMax := c.RateSeconds
				for _, entry := range dirEntries {
					if !entry.IsDir() {
						fileName := entry.Name()
						keys, values, err := collectors.GetValuesFromFile(filePath, fileName, lastRecordTime,
							rateSecondsMax, currentTime, keys, parser)
						if err != nil {
							continue
						}

						c.Records[collectors.RecordTime(fileName)] = values
						c.Keys = keys
					}
				}
			} else if c.ReaderStarted && !c.ParserStarted {
				mainParser = true
				c.ParserStarted = true
			}
			c.Unlock()
		case <-stop:
			c.Lock()
			c.ParserStarted = false
			c.Unlock()
			return nil
		}
	}
}

func (c *Collector) GetStat(rateSeconds int) (*proto.SysinfoResponse, error) {
	currentTimeString := time.Now().Format(time.DateTime)
	currentTimeStringCleaned := strings.ReplaceAll(strings.ReplaceAll(strings.ReplaceAll(currentTimeString,
		"-", ""), " ", ""), ":", "")
	currentTime, _ := collectors.GetRecordTimeFromString(currentTimeStringCleaned)
	c.Lock()
	avgValues := make(collectors.Values)
	var valuesQty int
	for recordTimeString, values := range c.Records {
		recordTime, err := collectors.GetRecordTimeFromString(string(recordTimeString))
		if err != nil {
			delete(c.Records, recordTimeString)
			continue
		}

		if recordTime.Compare(currentTime) <= 0 && currentTime.Sub(recordTime) <= time.Second*time.Duration(rateSeconds) {
			valuesQty++
			rawValues := *values
			for key, value := range rawValues {
				avgValues[key] += value
			}
			continue
		}

		// Удаляем неактуальную запись (если она не входит в максимальный диапазон по всем запросам клиентов).
		if recordTime.Compare(currentTime) > 0 || currentTime.Sub(recordTime) > time.Second*time.Duration(c.RateSeconds) {
			delete(c.Records, recordTimeString)
		}
	}
	keys := c.Keys
	c.Unlock()

	for key, value := range avgValues {
		avgValues[key] = value / collectors.Value(valuesQty)
	}

	msg := &proto.SysinfoResponse{
		Time:   timestamppb.New(currentTime),
		Keys:   collectors.GetStringSliceFromKeySlice(keys),
		Values: collectors.GetFloat32MapFromValueMap(avgValues),
	}

	return msg, nil
}

func New() *Collector {
	records := make(collectors.Records)
	return &Collector{
		Records: records,
		Keys:    []collectors.Key{},
	}
}

func parser(file *os.File, keys []collectors.Key) ([]collectors.Key, *collectors.Values, error) {
	scanner := bufio.NewScanner(file)

	for i := 0; i < 3; i++ {
		scanner.Scan()
	}

	i := 0
	values := make(collectors.Values)

	for scanner.Scan() {
		splittedLine := strings.Fields(scanner.Text())

		if len(splittedLine) == 0 {
			continue
		}

		keys = append(keys, collectors.Key(splittedLine[0]+" tps"))
		parsedValue, err := strconv.ParseFloat(splittedLine[1], 32)
		if err != nil {
			return keys, nil, logger.ErrGettingValuesFromFile
		}

		values[keys[i]] = collectors.Value(parsedValue)

		keys = append(keys, collectors.Key(splittedLine[0]+" RkBs"))
		parsedValue, err = strconv.ParseFloat(splittedLine[2], 32)
		if err != nil {
			return keys, nil, logger.ErrGettingValuesFromFile
		}

		values[keys[i+1]] = collectors.Value(parsedValue)

		keys = append(keys, collectors.Key(splittedLine[0]+" WkBs"))
		parsedValue, err = strconv.ParseFloat(splittedLine[3], 32)
		if err != nil {
			return keys, nil, logger.ErrGettingValuesFromFile
		}

		values[keys[i+2]] = collectors.Value(parsedValue)
		i += 3
	}

	return keys, &values, nil
}
