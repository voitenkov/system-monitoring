package collectors

type (
	RecordTime string
	Key        string
	Value      float32
	Values     map[Key]Value
	Records    map[RecordTime]*Values
)
