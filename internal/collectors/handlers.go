package collectors

import (
	"bufio"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"

	"gitlab.com/voitenkov/system-monitoring/internal/logger"
)

type ParserFunc func(file *os.File, keys []Key) ([]Key, *Values, error)

func Reader(collectorName, command string) (recordTime string, err error) {
	var output []byte
	filePath := "/tmp/" + collectorName
	err = os.MkdirAll(filePath, 0o777)
	if err != nil {
		return recordTime, logger.ErrCreatingDirectory
	}

	time := time.Now().Format(time.DateTime)
	recordTime = strings.ReplaceAll(strings.ReplaceAll(strings.ReplaceAll(time, "-", ""), " ", ""), ":", "")
	toFile, err := os.Create(filepath.Join(filePath, recordTime))
	if err != nil {
		return recordTime, logger.ErrCreatingFile
	}

	defer toFile.Close()

	buf := bufio.NewWriter(toFile)
	commandSequence := strings.Split(command, ";")
	for i := range commandSequence {
		splittedCommand := strings.Split(commandSequence[i], " ")

		switch len(splittedCommand) {
		case 0:
			return recordTime, logger.ErrEmptyCommand
		case 1:
			commandName := splittedCommand[0]
			output, err = exec.Command(commandName).Output()
		default:
			commandName := splittedCommand[0]
			commandArgs := splittedCommand[1:]
			output, err = exec.Command(commandName, commandArgs...).Output()
		}

		if err != nil {
			err = logger.ErrRunningCommand
			break
		}

		_, err = buf.Write(output)
		if err != nil {
			err = logger.ErrRunningCommand
			break
		}
	}

	buf.Flush()

	return recordTime, err
}

func GetValuesFromFile(filePath, fileName, lastRecordTime string, rateSecondsMax int, currentTime time.Time,
	keys []Key, fn ParserFunc,
) ([]Key, *Values, error) {
	if fileName == lastRecordTime {
		return keys, nil, logger.ErrGettingValuesFromFile
	}

	recordTime, err := GetRecordTimeFromString(fileName)
	if err != nil {
		return keys, nil, logger.ErrGettingValuesFromFile
	}

	filePathWithName := filepath.Join(filePath, fileName)
	defer os.Remove(filePathWithName)

	if recordTime.Compare(currentTime) > 0 || currentTime.Sub(recordTime) > time.Second*time.Duration(rateSecondsMax) {
		return keys, nil, logger.ErrGettingValuesFromFile
	}

	fileInfo, err := os.Stat(filePathWithName)
	if err != nil {
		return keys, nil, logger.ErrGettingValuesFromFile
	}

	if fileInfo.Size() == 0 {
		return keys, nil, logger.ErrGettingValuesFromFile
	}

	file, err := os.Open(filePathWithName)
	if err != nil {
		return keys, nil, logger.ErrGettingValuesFromFile
	}

	defer file.Close()

	return fn(file, keys)
}

func GetRecordTimeFromString(fileName string) (time.Time, error) {
	timeFormatted := fileName[:4] + "-" + fileName[4:6] + "-" + fileName[6:8] + " " + fileName[8:10] + ":" +
		fileName[10:12] + ":" + fileName[12:14]
	return time.Parse(time.DateTime, timeFormatted)
}

func GetStringSliceFromKeySlice(keys []Key) []string {
	stringSlice := make([]string, 0, len(keys))
	for _, key := range keys {
		stringSlice = append(stringSlice, string(key))
	}
	return stringSlice
}

func GetFloat32MapFromValueMap(values Values) map[string]float32 {
	float32Map := make(map[string]float32)
	for key, value := range values {
		float32Map[string(key)] = float32(value)
	}
	return float32Map
}
