package initcollectors

import (
	"gitlab.com/voitenkov/system-monitoring/api/proto"
	"gitlab.com/voitenkov/system-monitoring/internal/app"
	"gitlab.com/voitenkov/system-monitoring/internal/collectors/cpuload"
	"gitlab.com/voitenkov/system-monitoring/internal/collectors/diskload"
	"gitlab.com/voitenkov/system-monitoring/internal/collectors/diskuse"
	"gitlab.com/voitenkov/system-monitoring/internal/collectors/sysload"
	"gitlab.com/voitenkov/system-monitoring/internal/config"
)

type Collector interface {
	StartReader(stop chan bool) error
	StartParser(stop chan bool, rateSeconds int) error
	GetStat() (*proto.SysinfoResponse, error)
	Stop(rateSeconds int) error
}

func NewSysloadCollector() app.Collector {
	return sysload.New()
}

func NewCpuloadCollector() app.Collector {
	return cpuload.New()
}

func NewDiskloadCollector() app.Collector {
	return diskload.New()
}

func NewDiskuseCollector() app.Collector {
	return diskuse.New()
}

func New(cfg *config.Config) (map[app.StatType]app.Collector, error) {
	collectorsSet := make(map[app.StatType]app.Collector)

	if cfg.Collectors.SysLoad.Enabled {
		collectorsSet["sys_load"] = NewSysloadCollector()
	}

	if cfg.Collectors.SysLoad.Enabled {
		collectorsSet["cpu_load"] = NewCpuloadCollector()
	}

	if cfg.Collectors.SysLoad.Enabled {
		collectorsSet["disk_load"] = NewDiskloadCollector()
	}

	if cfg.Collectors.SysLoad.Enabled {
		collectorsSet["disk_use"] = NewDiskuseCollector()
	}

	return collectorsSet, nil
}
