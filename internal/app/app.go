package app

import (
	"gitlab.com/voitenkov/system-monitoring/api/proto"
	"gitlab.com/voitenkov/system-monitoring/internal/logger"
)

type StatType string

type App struct {
	collectors map[StatType]Collector
}

type Collector interface {
	StartReader(stop chan bool) error
	StartParser(stop chan bool, rateSeconds int) error
	GetStat(rateSeconds int) (*proto.SysinfoResponse, error)
}

func (a *App) StartReader(statType StatType, stop chan bool) error {
	collector := a.collectors[statType]
	return collector.StartReader(stop)
}

func (a *App) StartParser(statType StatType, stop chan bool, rateSeconds int) error {
	collector := a.collectors[statType]
	return collector.StartParser(stop, rateSeconds)
}

func (a *App) GetStat(statType StatType, rateSeconds int) (*proto.SysinfoResponse, error) {
	collector := a.collectors[statType]
	return collector.GetStat(rateSeconds)
}

func (a *App) CheckCollector(statType StatType) error {
	_, found := a.collectors[statType]
	if !found {
		return logger.ErrCollectorIsNotEnabled
	}

	return nil
}

func New(collectors map[StatType]Collector) *App {
	return &App{
		collectors: collectors,
	}
}
