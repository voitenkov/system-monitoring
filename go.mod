module gitlab.com/voitenkov/system-monitoring

go 1.21.6

require (
	github.com/sirupsen/logrus v1.9.3
	github.com/stretchr/testify v1.7.0
	google.golang.org/grpc v1.63.2
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
)

require (
	github.com/gofrs/uuid v4.4.0+incompatible
	golang.org/x/net v0.21.0 // indirect
	golang.org/x/sys v0.17.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20240227224415-6ceb2ff114de // indirect
	google.golang.org/protobuf v1.33.0
)
